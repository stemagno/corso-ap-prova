package Rettangolo;

public class Rettangolo {

	/*ESERCIZIO Rettangolo
	- due variabili private di tipo double: "base" e "altezza"
	- costruttore con due parametri "b" e "a" che inizializza l'oggetto
	- metodi getBase() e getAltezza() per ottenere la base e l'altezza
	- (nessun metodo set: l'oggetto � immutabile)
	- metodo toString() che stampi info. utili sull'oggetto
	Fare dei test con pi� oggetti diversi per verificare che tutto funzioni
	sempre come previsto.
	- metodo getPerimetro() che calcola il perimetro del rettangolo (valore double)
	  (perimetro = somma di tutti i lati del rettangolo)
	- metodo getArea() che calcola l'area del rettangolo (valore double)
	  la formula �: area = base * altezza
	Ripetere i test.*/

	private double base;
	private double altezza;
	
	public Rettangolo ( double basic, double height) {
		base= basic;
		altezza = height;
				
	} public String toString() {
		String info = "base"+base+"altezza"+altezza+"";
		return info;
		
		
	} public double getArea() { 
		return base*altezza;
		
	} public double getPerimetro() {
		return base+base+altezza+altezza;
		
	}
}
