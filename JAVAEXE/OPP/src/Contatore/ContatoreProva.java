package Contatore;

public class ContatoreProva {
			//attributo
			private int valore;
			
			//COSTRUTTORE
			public ContatoreProva() {
					valore = 0;
			}
			
			//metodo get
			public int getValore() {
				return valore++;
			}
		
			public int incrementa() {
				   valore++;
				  return valore;
			}
					
			public int  reset() {
					valore=0;
				return valore;
			} 
			
	}		