package Contatore;

public class exContatore {
	
	
	/*	Provare a stampare un oggetto di tipo contatore. Cosa vediamo?
		Aggiungere un opportuno metodo toString() che ha
			- nessun parametro in ingresso
			- tipo di ritorno String
		Il compito del metodo � quello di restituire al chiamante
		una rappresentazione testuale (una stringa) che rappresenta l'oggetto.
		Ad esempio, il metodo potrebbe produrre una stringa simile a questa:
		"Contatore a X", dove X � il valore corrente.
		Riprovare a stampare l'oggetto. Cosa vediamo adesso?
		Qual �, quindi, il compito della funzione toString()?
		Fare dei test instanziando pi� oggetti di tipo contatore. Verificare che
		gli oggetti sono indipendenti fra di loro. Ad esempio, se ne incremento uno,
		gli altri restano invariati.
		Aggiungere un metodo setValore(int x) per il contatore che faccia
		saltare a x
		il conteggio SOLO SE x � positivo. Ad esempio, posso impostare
		il mio contatore
		a 50, ma non a -50.
		Fare dei test per verificare che tutto funzioni come previsto.	*/	
	
	 //attributi

	private int valore;
	
	public exContatore() {
		valore = 0;
	}
	
	public int getValore() {
		return valore;
	}
	
	public void setValore(int valore) {
		if (valore > 0) {
			this.valore = valore;
		}
	}
	
	public void incrementa() {
		valore += 1;
	}
	
	public void reset() {
		valore = 0;
	}
	
	@Override
	public String toString() {
		return "Contatore a " + valore;
	}
	
	public boolean equals(exContatore that) {
		return this.valore == that.valore;
	}

}