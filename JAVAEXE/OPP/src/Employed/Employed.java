package Employed;

public class Employed {


		/*ESERCIZIO 4 IMPIEGATO
		- variabili private
		   - id (intero)
		   - nome (stringa)
		   - cognome (stringa)
		   - stipendio (int)
		- costruttore che riceve tre parametri
		   - "id" (intero)
		   - "nome" (stringa)
		   - "cognome" (stringa)
		   e inizializza i campi dell'oggetto.
		   PROBLEMA: alcuni parametri hanno lo stesso nome delle variabili. Come faccio
		   a inizializzare l'oggetto? Perch�, ad esempio, "id = id;" non funziona?
		   SOLUZIONE: la parola chiave "this" rappresenta l'oggetto corrente
		      "id"      -> � il parametro del costruttore
		      "this.id" -> � il campo dell'oggetto
		- il costruttore inizializza id, nome e cognome al valore specificato; inoltre,
		  stabilisce lo stipendio iniziale a 1000
		- inserire metodi getter (getId(), getNome(), ecc.) per leggere ogni propriet�
		  dell'oggetto (inserire il tipo di ritorno opportuno).
		- aggiungere un metodo toString() per ottenere una rappresentazione testuale
		  dell'oggetto.
		- aggiungere un metodo aumentaStipendio() che possiamo utilizzare per
		  concedere un aumento al dipendente. Il metodo non ritorna alcun valore (void)
		  e prende come parametro in ingresso una variabile intera "x" che rappresenta
		  la quantit� dell'aumento.		Fare dei test per verificare che ogni metodo funzioni come previsto.
		Controllare inoltre che oggetti diversi hanno uno stato indipendente.*/
	
			private int id;
			private String name;
			private String surname;
			double check ;
			
			
			
			public Employed ( int id, String name, String surname, double check, double upgradecheck) {
				this.id=id;
				this.name = name;
				this.surname = surname;
				this .check=1000;
			//	double upgradecheck; //posso anche passare il valore al costruttoree inizializzarlo??
				
			} public int getId () {
				return id;
				
			} public String getname () {
				return name;
				
				
			} public String getsurname() {
				return surname;
				
				
			} public double getcheck () {
				return check;
				
			} public void  upgradecheck (double upgradecheck ) {
				//check += upgradecheck; */ //potevo anche mettere un metodo get ???
				this.check += upgradecheck;
				
			} public String toString ( ) {
					return " employed " + this.id + " name" + this.name + " and surname" + this.surname
							+ " that earn " + this.check ;
			}
			
		}
		

