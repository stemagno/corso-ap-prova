  package InvoiceItem;

public class InvoiceItem2 {
	
	/*ESERCIZIO 5:InvoiceItem
	Scrivere una classe Java chiamata InvoiceItem che rappresenta una
	 voce di acquisto all'interno di una fattura.
{id = "2K651", desc = "Pennelli Cinghiale", qty = 5, unitPrice = 8.90}
	La classe prevede un solo costruttore che prende come parametro
	- id
	- desc
	- qty
	- unitPrice
	e inizializza l'oggetto nel modo naturale.
	La classe espone metodi getter pubblici per ogni campo privato.
	Una volta implementati i getter, scrivere un programma di prova
	 per verificare il funzionamento di tutti. 
	 
	 Costruire un oggetto, recuperarne le propriet� tramite
	i rispettivi metodi getter e stampare a video i risultati.
	
	Aggiungere il metodo toString() per stampare a video una
	 rappresentazione testuale come questa:
	  "InvoiceItem[id=...,desc=...,qty=...,unitPrice=...]"
	Provare subito il metodo toString() sullo stesso oggetto di prova.
	
	AggIungere un metodo setUnitPrice(...) che consente di modificare
	 il prezzo unitario dei prodotti.
	  Provare subito il corretto funzionamento del metodo.
	  
	Aggiungere un metodo getTotal() che calcola e restiuisce il
	 costo totale della voce di spesa.
	  Ricorda che la voce � composta da un prezzo e una quantit�.
	  
	Provare il funzionamento anche di questo metodo.
	Aggiungere un metodo equals(...) che prende in ingresso un
	 altro oggetto di tipo InvoiceItem e restituisce un boolean:
	- true, se i due oggetti hanno stesso id
	- false, altrimenti
	Attenzione al confronto tra le stringhe! 
	Gli id sono stringhe, e le stringhe
	sono oggetti. */
	
	private String id;
	private String desc;
	int qty;
	double UnitPrice;
	
	public InvoiceItem2 ( String id, String desc, int qty, double UnitPrice )	 {
	
		this.id = id;
		this.desc = desc;
		this.qty = qty;
		this.UnitPrice= UnitPrice;
		
	} public String getid () {
		return this.id;
		
	}public String getdesc () {
		return this.desc;
		
	}public int getqty () {
		return this.qty;
		
	}public double getUnitPrice () {
		return this.UnitPrice;
		
	} public String toString () {
		return "info: [code: " +id+ ". Desc: " +desc+ ". x   Qty: " +qty+ ". UnitPrice: " +UnitPrice+"]";
		
	}/* AggIungere un metodo setUnitPrice(...) che consente
	 di modificare il prezzo unitario dei prodotti.
	  Provare subito il corretto funzionamento del metodo. */
	public double setUnitPrice ( int UnitPrice2 ) {
		return this.UnitPrice = UnitPrice2;
		
		//??
	}
	/*Aggiungere un metodo getTotal() che calcola e restiuisce il
	 costo totale della voce di spesa.
	  Ricorda che la voce � composta da un prezzo e una quantit�.*/
	public double getTotal () {
		return UnitPrice * qty;
		
	} /*	Aggiungere un metodo equals(...) che prende in ingresso un
	 altro oggetto di tipo InvoiceItem e restituisce un boolean:
	- true, se i due oggetti hanno stesso id
	- false, altrimenti*/
	
	public boolean equals ( InvoiceItem item2 ) {
		if (this.id.equals(that.id)){
			
			return true; 
		} else {
			return false;
		}/* Attenzione al confronto tra le stringhe! 
		Gli id sono stringhe, e le stringhe
		sono oggetti. */           //???
	}
	
}
