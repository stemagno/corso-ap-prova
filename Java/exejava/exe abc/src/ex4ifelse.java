
import java.util.Scanner;

public class ex4ifelse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	/* Esercizio 4
Scrivi un programma chiamato GiornoInParola che 
converta un numero nel corrispondente giorno della settimana. 
Una variabile �giorno� ospita un numero da 1 a 7. 
Il programma deve stampare �luned� se il numero � 1,
�marted� se il numero � 2, e cos� via fino a �domenica� 
quando il numero � 7; altrimenti, stampa �giorno non valido�.
Estensione: estendi il programma affinch� stampi anche:
�feriale� se si tratta di un giorno tra �luned� e �venerd�
�prefestivo� per il sabato
�festivo� per la domenica
Estensione: chiedere il giorno in input all�utente. */
		
		Scanner input = new Scanner (System.in);

		System.out.println ("Inserire un numero da 1 a 7");
		
		int giorno = input.nextInt();
		
		if ( giorno == 1 ) {
			System.out.println ("Il giorno � Luned�");
			System.out.println ("� un giorno feriale");
		} if (giorno ==2) { 
			System.out.println ("Il giorno � Marted�");
			System.out.println (" Feriale");
		} if (giorno ==3) { 
			System.out.println ("Il giorno � Mercoled�");
			System.out.println (" Feriale");
		} if (giorno ==4) {
			System.out.println ("Il giorno � Gioved�");
			System.out.println (" Feriale");
		} if (giorno ==5) {
			System.out.println ("Il giorno � Venerd�");
			System.out.println (" Prefestivo");
		} if (giorno ==6) {
			System.out.println ("Il giorno � Sabato");
			System.out.println (" Festivo");
		} else if (giorno ==7) {
			System.out.println ("Il giorno � Domenica");
			System.out.println (" Festivo");
		} else  { 
			System.out.println ("Il giorno non � valido");
		}
		
	}

}
