
import java.util.Scanner;

public class ex7forwhile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// ES 6
	/*	Scrivi un programma per calcolare le tabelline. Il programma si
	 *  chiama Tabelline. Il programma chiede in input un numero all�utente
	 *   e poi stampa, riga per riga, la tabellina di quel numero.
	 *    Ad esempio, se l�utente inserisce 6: */
		/*    6 * 0 = 0
				6 * 1 = 6
				6 * 2 = 12
				6 * 3 = 18
				6 * 10 = 60 */
		

	/*	Esercizio 7
			Estendi il programma dell�Esercizio 6 affinch�,
prima di terminare, chieda all�utente se vuole eseguire di nuovo.
Suggerimento: si potrebbe procedere in questo modo: */
		
		boolean ripeti ;
		
		do { 

		Scanner input1 = new Scanner (System.in);
		
		System.out.println ("Calcola la tabelli de numero inserito");
		
		int tab = input1.nextInt();
		int ris;
		for (int i=0; i<11; i++) {
			
			ris = i*tab;
			
			System.out.println (+i+"*"+tab+"="+ris+"");
			
		}System.out.println (" vuoi eseguire di nuovo?");
		
		String risposta = input1.next().toLowerCase();
		
		if (risposta.equals("si")) {
			
			ripeti = true;
			
		} else {
			
			ripeti = false;
			
			}  
		}  while (ripeti);
	}
}

