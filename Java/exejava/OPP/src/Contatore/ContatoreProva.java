package Contatore;

public class ContatoreProva {

			private int valore;
			
			public ContatoreProva() {
				valore=0;
			}
		
			public int getValore() {
				return valore;
			}
			
			public void incrementa() {
				valore++;
			}
					
			public void reset() {
				valore =0;		
			}
	}		
}			